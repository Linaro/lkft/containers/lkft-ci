FROM python:3.8-slim-buster
LABEL maintainer="Justin Cook <justin.cook@linaro.org>"

ARG DEBIAN_FRONTEND=noninteractive

WORKDIR /

# hadolint ignore=DL3008
RUN apt-get update && apt-get install -y --no-install-recommends \
        git \
        jq \
        openssh-client \
        wget \
        && apt-get clean \
        && rm -rf /var/lib/apt/lists/*
